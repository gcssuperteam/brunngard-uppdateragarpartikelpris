﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UppdateraGarpArtikelPris
{
    class CLogger
    {
        //Kontrollerar vilken typ av loggning som ska göras
        public bool WriteToLog(string in_s_ErrorPart)
        {
            WriteToLogfile(in_s_ErrorPart);
            return true;
        }

        //Kontrollerar vilken typ av loggning som ska göras
        public bool WriteToLog(Exception o_ExceptionMess, string in_s_ErrorPart)
        {
            WriteToLogfile(o_ExceptionMess, in_s_ErrorPart);

            return true;
        }


        //Skriver till loggfil med exception som inparameter
        public bool WriteToLogfile(Exception in_o_ErrorMessage, string in_s_ErrorPart)
        {
            System.IO.FileStream o_FS = default(System.IO.FileStream); ;
            System.IO.StreamWriter o_SR = default(System.IO.StreamWriter);
            string s_File = "";
            DateTime d_LogDate = DateTime.Now;

            s_File = Properties.Settings.Default.LogFilePath;

            //Om filen inte finns så skapas den
            if (!File.Exists(s_File))
            {
                try
                {
                    o_FS = new System.IO.FileStream(Properties.Settings.Default.LogFilePath,
                                System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);

                }
                catch (Exception)
                {
                    return false;
                }
            }
            //Om filen redan finns
            else
            {
                o_FS = new System.IO.FileStream(Properties.Settings.Default.LogFilePath,
                               System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.None);

            }
            //ANSI
            o_SR = new System.IO.StreamWriter(o_FS, System.Text.Encoding.GetEncoding(1252));
            o_SR.WriteLine(d_LogDate.ToString() + in_s_ErrorPart + " " + in_o_ErrorMessage);
            //Skriver även till Console
            //Console.WriteLine(d_LogDate.ToString() + in_s_ErrorPart + " " + in_o_ErrorMessage);

            o_SR.Close();

            o_SR = null;
            o_FS = null;

            return true;
        }

        //Skriver till loggfil utan exception som inparameter
        public bool WriteToLogfile(string in_s_ErrorPart)
        {
            System.IO.FileStream o_FS = default(System.IO.FileStream);
            System.IO.StreamWriter o_SR = default(System.IO.StreamWriter);
            string s_File = "";
            DateTime d_LogDate = DateTime.Now;

            s_File = Properties.Settings.Default.LogFilePath;

            //Om filen inte finns så skapas den
            if (!File.Exists(s_File))
            {
                try
                {
                    o_FS = new System.IO.FileStream(Properties.Settings.Default.LogFilePath,
                                System.IO.FileMode.Create, System.IO.FileAccess.Write, System.IO.FileShare.None);

                }
                catch (Exception)
                {
                    return false;
                }
            }
            //Om filen redan finns
            else
            {
                o_FS = new System.IO.FileStream(Properties.Settings.Default.LogFilePath,
                               System.IO.FileMode.Append, System.IO.FileAccess.Write, System.IO.FileShare.None);

            }

            //ANSI
            o_SR = new System.IO.StreamWriter(o_FS, System.Text.Encoding.GetEncoding(1252));
            o_SR.WriteLine(d_LogDate.ToString() + in_s_ErrorPart);
            //Skriver även till Console
            //Console.WriteLine(d_LogDate.ToString() + in_s_ErrorPart);
            o_SR.Close();

            o_SR = null;
            o_FS = null;

            return true;
        }
    }
}
