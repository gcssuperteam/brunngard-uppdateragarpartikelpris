﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace UppdateraGarpArtikelPris
{
    class CGarpFunction
    {
        public void UppdateraArtikelPris(bool test)
        {
            CGarpSystem o_GarpSys = new CGarpSystem();
            CLogger _logger = new CLogger();            
            List<CPricelist> _artikelLista = new List<CPricelist>();
            List<CPricelist> _outletArtikelLista = new List<CPricelist>();
            
            Garp.ITable o_APP = null;
            Garp.ITable o_APP_outlet = null;
            Garp.ITable o_AGA = null;
            Garp.ITable o_OGR = null; //Orderrad
            Garp.ITable o_OGF = null; //Orderhuvudtext
            Garp.ITable o_KP = null; //L-kundprislista
            string s_prislista = string.Empty;
            string s_prislista_outlet = string.Empty;
            double d_pris = 0;
            DateTime todaysDate = DateTime.Today;
            string dateFrom = String.Empty;
            string dateTo = String.Empty;
            string index_outlet = String.Empty;
            string index_standard = String.Empty;
            string decimals = string.Empty;
            int testIndex = 0;
            List<string> artNrList = new List<string>();

            o_GarpSys.Username = Properties.Settings.Default.GarpUser;
            o_GarpSys.Password = Properties.Settings.Default.GarpPassword;

            s_prislista = Properties.Settings.Default.Prislista;
            s_prislista_outlet = Properties.Settings.Default.PrislistaOutlet;
            bool mDetailedLog = Properties.Settings.Default.DetailedLog;

            string testArtNr = Properties.Settings.Default.TestArtNr;

            

           
            //Kolla om artiklar finns i outletPrislistan. Finns inte artikeln ska priset uppdateras som vanligt. 
            
            try
            {
                if (o_GarpSys.Login(ref o_GarpSys.o_Garp))
                {

                    //Kör med testartiklar angivna i settings.
                    if (test == true && string.IsNullOrEmpty(testArtNr) == false)
                    {
                        artNrList = testArtNr.Split(';').ToList<string>();
                    }         

                  
                    //getOrders();
                    o_APP = o_GarpSys.o_Garp.Tables.Item("APP");
                    o_APP_outlet = o_GarpSys.o_Garp.Tables.Item("APP");
                    o_OGR = o_GarpSys.o_Garp.Tables.Item("OGR");
                    o_OGF = o_GarpSys.o_Garp.Tables.Item("OGF");
                    o_KP = o_GarpSys.o_Garp.Tables.Item("KP"); //L-kundprislistan
                    o_AGA = o_GarpSys.o_Garp.Tables.Item("AGA"); //Artikeltabellen
                   
                    o_APP.IndexNo = 4;
                    o_APP_outlet.IndexNo = 4;
                    o_OGR.IndexNo = 4;

                    string standard_price = "";
                    string outlet_price = "";                    
                   
                    o_APP.Find(s_prislista.PadRight(10));
                    o_APP_outlet.Find(s_prislista_outlet.PadRight(10));
                
                    o_APP.Next();
                    o_APP_outlet.Next();
                    o_AGA.Next();       //Artikeltabellen            

                    while (!o_AGA.Eof)
                    {
                        CPricelist o_PriceList = new CPricelist();

                        //Kör med testartikel om setting = TRUE
                        if (test == true && artNrList.Count > 0)
                        {
                            if (testIndex < artNrList.Count)
                            {
                                o_PriceList.Artikelnummer = artNrList[testIndex].ToString();

                                //Hämta antal prisdecimaler från artikelkortet
                                if (o_AGA.Find(o_PriceList.Artikelnummer))
                                    decimals = o_AGA.Fields.Item("PDE").Value; 
                               
                                testIndex++;
                            }
                            else
                                break; //Har kört igenom testartiklarna. Bryt loopen.                             
                        }
                        else
                        {
                            o_PriceList.Artikelnummer = o_AGA.Fields.Item("ANR").Value;
                            decimals = o_AGA.Fields.Item("PDE").Value.ToString();
                        }
                      
                        index_standard = "12" + o_PriceList.Artikelnummer.PadRight(13, ' ') + " A" + s_prislista.PadRight(10, ' ') + "5";
                        index_outlet = "12" + o_PriceList.Artikelnummer.PadRight(13, ' ') + " A" + s_prislista_outlet.PadRight(10, ' ') + "5"; //Index för prisalternativ 5

                        // ********************************************************************************************************
                        // ********************************************************************************************************
                        if (test == true)
                            _logger.WriteToLog(" Kört artikelnummer: " + o_PriceList.Artikelnummer.ToString());

                        if (decimals.Equals("2") || decimals.Equals("3") || decimals.Equals("4") || decimals.Equals("5"))
                        {
                            if (o_KP.FindReal(index_outlet) == true)
                                outlet_price = ConvertDecimal(o_KP.Fields.Item("NU2").Value.ToString(), decimals);
                            if (o_KP.FindReal(index_standard) == true)
                                standard_price = ConvertDecimal(o_KP.Fields.Item("NU2").Value.ToString(), decimals);
                        }                  
                        else if (o_AGA.Fields.Item("PDE").Value.ToString().Equals("0"))
                        {
                            if(mDetailedLog)
                                _logger.WriteToLog("Artikel " + o_AGA.Fields.Item("ANR").Value.ToString() + " hade 0 decimaler som inställning för prisfältet på artikelkortet");
                        }

                        // ********************************************************************************************************
                        // ********************************************************************************************************

                         //Sök efter artikeln på prisalternativ 5 i outletprislistan
                        if (o_KP.FindReal(index_outlet) == true)
                        {
                            dateFrom = o_KP.Fields.Item("GD1").Value;
                            dateTo = o_KP.Fields.Item("GD2").Value;

                            //Om datumen är satta skall standardpris användas. (Kampanjpris får ej sättas på artikelkort)
                            if (String.IsNullOrEmpty(dateFrom) == false && String.IsNullOrEmpty(dateTo) == false)
                            {                      
                                o_PriceList.Pris = standard_price;
                                _artikelLista.Add(o_PriceList);
                            }
                            else //Det fanns inget datumintervall. Använd outletpris
                            {
                                o_PriceList.Pris = outlet_price;
                                _artikelLista.Add(o_PriceList);
                            }

                        }                 
                        else //Artikeln fanns inte som ett alt 5 i outlet, ta standardpris
                        {
                            if (o_KP.FindReal(index_standard) == true)
                            {
                                o_PriceList.Pris = standard_price;
                                _artikelLista.Add(o_PriceList);                                
                            }                           
                        }

                        if (!String.IsNullOrEmpty(o_PriceList.Pris) && mDetailedLog)
                            _logger.WriteToLog("Uppdaterar: " + o_PriceList.Artikelnummer + " Pris: " + o_PriceList.Pris);

                        o_AGA.Next();  //Nästa artikel                   
                        

                    }                  

                    _logger.WriteToLog(string.Format(" Lista skapad med artiklar och priser från prislistorna. Uppdaterar " + _artikelLista.Count.ToString() +" st artiklar"));

                    o_AGA = o_GarpSys.o_Garp.Tables.Item("AGA");

                    foreach (var _artLista in _artikelLista)
                    {
                        //uppdatera Garp  
                        if (o_AGA.Find(_artLista.Artikelnummer))
                        {
                            o_AGA.Fields.Item("PRI").Value = _artLista.Pris;
                            o_AGA.Post();
                        }
                    }
                    _logger.WriteToLog(string.Format(" Artikelpris uppdaterat i Garp"));
                }
                else
                {
                    _logger.WriteToLog(string.Format(" Kan inte logga in i Garp"));
                }
            }

            catch (Exception e)
            {
                _logger.WriteToLog(e, string.Format("Fel vid uppdatering av pris"));
            }

            finally
            {
                //Släng objekt
                o_APP = null;
                o_AGA = null;
                o_GarpSys.Dispose();
                o_GarpSys = null;
            }          
        }

        private string ConvertDecimal(string price, string decimals)
        {
            CLogger _logger = new CLogger();
            int no_decimal = 0;
            int index;

            try
            {
                no_decimal = Int32.Parse(decimals);

                decimal temp = 0;

                //Kolla så inte priset är 0
                if (price.StartsWith("0") == false)
                {
                    if (Decimal.TryParse(price.Replace('.', ','), out temp))
                    {
                        index = price.IndexOf('.');
                        price = price.Remove(price.IndexOf('.'), 1);
                        price = price.Insert(price.Length - no_decimal, ".");
                    }                  
                    
                }
                
            }
            catch (Exception ex)
            {
                _logger.WriteToLog(ex, string.Format(" Fel vid decimalkonvertering"));
            }

            return price;
        }



        public void getOrders()
        {
            CGarpSystem o_GarpSys = new CGarpSystem();


            
           // o_GarpSys.Login(ref o_GarpSys.o_Garp);
            List<string> orderNr = new List<string>();
            List<string> orderRader = new List<string>();
           
            Garp.ITable o_OGA = null; //Orderhuvud
            o_OGA = o_GarpSys.o_Garp.Tables.Item("OGA");

            Garp.ITable o_OGR = null; //Orderhuvud
            o_OGR = o_GarpSys.o_Garp.Tables.Item("OGR"); 
          
               string rad = "";
            

            //Alla ordernummer
            while (!o_OGA.Eof)
            {
                if (o_OGA.Fields.Item("ONR").Value != null)
                {
                    orderNr.Add(o_OGA.Fields.Item("ONR").Value);                   
                }             
                
                o_OGA.Next();
            }
            
             

            //Hämta rader för alla ordrar
            foreach(string order in orderNr)
            {
                o_OGR.Find(order);
                
                while (!o_OGR.Eof)
                {
                    rad = o_OGR.Fields.Item("RDC").Value;
                    Console.WriteLine(rad);
                    o_OGR.Next();
                }
                
            }
                

            while (!o_OGR.Eof)
            {
                if (o_OGR.Fields.Item("ONR").Value != null)
                {
                    orderRader.Add(o_OGA.Fields.Item("ONR").Value);
                }

                o_OGR.Next();
            }

           

           
        
        }
    }
}
