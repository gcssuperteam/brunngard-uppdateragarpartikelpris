﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UppdateraGarpArtikelPris
{
    class CGarpSystem : IDisposable
    {
        public Garp.Application o_Garp;

        public CGarpSystem()
        {
            ////default values
            _username = "";
            _password = "";
            ////create new com object
            o_Garp = new Garp.Application();
        }
        public CGarpSystem(string username, string password)
        {
            ////default values
            _username = username;
            _password = password;
            ////create new com object
            o_Garp = new Garp.Application();
        }

        public bool Login(ref Garp.Application in_o_Garp)
        {
            return _login(ref in_o_Garp, _username, _password);
        }
        public bool Login(ref Garp.Application in_o_Garp, string in_s_Username, string in_s_Password)
        {
            _username = in_s_Username;
            _password = in_s_Password;
            return _login(ref in_o_Garp, in_s_Username, in_s_Password);
        }
        private bool _login(ref Garp.Application in_o_Garp, string in_s_Username, string in_s_Password)
        {
            bool b_Ok = true;
            ////Login if Garp isn't running
            if ((in_o_Garp.User == null) && (!"".Equals(in_s_Username)) && (!"".Equals(in_s_Password)))
            {
                ////Try to login
                try
                {
                    b_Ok = in_o_Garp.Login(in_s_Username, in_s_Password);
                }
                catch (Exception e)
                {
                    b_Ok = false;
                }
            }
            else if (in_o_Garp.User != null && in_o_Garp.User.Equals(in_s_Username))
            {
                ////If Garp is running use that session
                b_Ok = true;
            }
            else
            {
                ////Failed
                b_Ok = false;
            }
            return b_Ok;
        }

        #region "Dispose"
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposedValue = false;
        protected virtual void Dispose(bool disposing)
        {
            //release com
            if ((!this.disposedValue))
            {
                if ((disposing))
                {
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(o_Garp);
                }
            }
            this.disposedValue = true;
        }
        #endregion

        #region "Props"
        #region "Declarations"
        private string _username;
        private string _password;
        private string _lasterror;
        #endregion
        #region "Properties"
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string Lasterror
        {
            get { return _lasterror; }
            set { _lasterror = value; }
        }
        #endregion
        #endregion

    }
}
