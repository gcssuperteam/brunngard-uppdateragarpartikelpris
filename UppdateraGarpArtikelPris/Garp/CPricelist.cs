﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UppdateraGarpArtikelPris
{
    public class CPricelist
    {
        private string _artikelnummer;
        private string _pris;

        public string Artikelnummer
        {
            get { return _artikelnummer; }
            set { _artikelnummer = value; }
        }

        public string Pris
        {
            get { return _pris; }
            set { _pris = value; }
        }
    }
}
