﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UppdateraGarpArtikelPris
{
    class Program
    {
        static void Main(string[] args)
        {
            CLogger _logger = new CLogger();
            CGarpFunction o_CGF = new CGarpFunction();

            string testArtNr = Properties.Settings.Default.TestArtNr;
            bool test = Properties.Settings.Default.Test;

            _logger.WriteToLog(string.Format(Environment.NewLine + " Uppdatering startad ***************************************"));               
          
            o_CGF.UppdateraArtikelPris(test);

            _logger.WriteToLog(string.Format(" Uppdatering klar ******************************************"));

            //Console.WriteLine("Uppdatering klar.");
            //Console.Read();
        }
    }
}
